var express = require('express'),
 	app = express();
var bodyParser = require('body-parser');
// configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/app'));
//app.use(express.static(__dirname + '/angular'));
app.use(express.static(__dirname + '/bower_components'));
//app.use(express.static(__dirname + '/vendors'));
app.use(express.static(__dirname + '/img'));



app.listen(3000, function() {
	console.log('server started at port: ' + 3000);
});

module.exports = app;