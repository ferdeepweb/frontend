'use strict';

angular.module('base')
  .directive('calendar', function($compile, $uibModal, $location){
    return {
      restrict: 'A',
      scope: {
        select: '&',
        actionLinks: '=',
        events: '='
      },
      link: function(scope, element, attrs) {
        var events = [];
        if ($location.search().userId) {
          scope.events.forEach(function(event) {

            var nightShift = event.shift_id === 2;
            var date = Date.parse(event.active_on);
            var startHour = new Date((nightShift ? 10*3600*1000: -1*3600*1000) + date);
            var endHour = new Date(startHour.getTime() + 12*3600*1000);

            events.push({
              title: event.team.team_type_id == 1 ? 'Primary' : 'Secondary',
              start: startHour,
              end: endHour,
              className: nightShift ? 'bgm-cyan' : 'bgm-green',
              data: event
            })
          });

        } else {
          console.log(scope.events);
          for (var date in scope.events) {
            for (var scheduele in scope.events[date]) {
              var event = {};
              event.title = scope.events[date][scheduele].team.team_type_id == 1 ? 'Primary' : 'Secondary';
              event.start = new Date((scheduele == 2 ? 10*3600*1000: -1*3600*1000) + Date.parse(date));
              event.end = new Date(event.start.getTime() + 12*3600*1000);
              event.className = (scheduele == 2) ? 'bgm-cyan' : 'bgm-green';
              event.data = scope.events[date][scheduele];
              event.data.shift = scheduele;
              events.push(event);
            }
          }
        }

        //Generate the Calendar
        var fullCalendarElement = element.fullCalendar({
          header: {
            right: '',
            center: 'prev, title, next',
            left: ''
          },

          theme: true,
          selectable: true,
          selectHelper: true,
          editable: true,
          events: events,

          //On Day Select
          select: function(start, end, allDay) {
            scope.select({
              start: start,
              end: end
            });

          },
          eventClick: function(calEvent, jsEvent, view) {

            var template = $location.search().userId ? '/views/template/editEvent.html' : '/views/template/editEventOffice.html';
            openEditEvent(calEvent, template);
          }
        });

        var moment = element.fullCalendar('getDate');
        moment = moment.format();

        function openEditEvent(calEvent, template) {
          var _modalInstance = $uibModal.open({
            animation: true,
            size: 'md',
            templateUrl: template,
            controller: function($scope, proxy) {
              $scope.event = calEvent;
              console.log($scope.event);

              $scope.close = function() {
                _modalInstance.dismiss('cancel');
              };
            }
          });
        }

        //Add action links in calendar header
        element.find('.fc-toolbar').append($compile(scope.actionLinks)(scope));
      }
    }
  })


  //Change Calendar Views
  .directive('calendarView', function(){
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.on('click', function(){
          $('#calendar').fullCalendar('changeView', attrs.calendarView);
        })
      }
    }
  });

