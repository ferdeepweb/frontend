(function() {

  'use strict';

  angular
    .module('base')
    .factory('proxy', proxy);

  function proxy(HttpUtilities, $window) {

    var model = {
      authLoginUser: authLoginUser,
      createUser: createUser,
      getOffices: getOffices,
      createOffice: createOffice,
      listAllOffices: listAllOffices,
      listUsersForOffice: listUsersForOffice,
      listAllUsers: listAllUsers,
      getCalendarForUserAndDate: getCalendarForUserAndDate,
      getCalendarForOfficeAndDate: getCalendarForOfficeAndDate,
      saveUserStatus: saveUserStatus,
      generateCalendar: generateCalendar,
      getAnalyticsReport: getAnalyticsReport
    };

    var serverLocation = 'http://er.ddns.net:3000/api/v1/';
    //var serverLocation = 'http://192.168.5.83:3000/api/v1/';


    function authLoginUser(user) {
      return HttpUtilities.post(serverLocation + 'auth/sign_in', user);
    }

    function getOffices() {
      return HttpUtilities.get(serverLocation + 'offices ');
    }

    function createUser(user) {
      return HttpUtilities.post(serverLocation + 'users', user);
    }

    function createOffice(newOffice) {
      return HttpUtilities.post(serverLocation + 'offices', newOffice);
    }

    function listAllOffices() {
      return HttpUtilities.get(serverLocation + 'offices');
    }

    function listUsersForOffice(officeId) {
      return HttpUtilities.get(serverLocation + 'offices/'+officeId+'/users');
    }

    function listAllUsers() {
      return HttpUtilities.get(serverLocation + 'users');
    }

    function getCalendarForUserAndDate(userId, year, month) {
      return HttpUtilities.get(serverLocation + 'schedules?year='+year+'&month='+month+'&user_id='+userId);
    }

    function getCalendarForOfficeAndDate(officeId, year, month) {
      return HttpUtilities.get(serverLocation + 'schedules?year='+year+'&month='+month+'&office_id='+officeId);
    }

    function saveUserStatus(statusId, date, userId) {
      return HttpUtilities.post(serverLocation + 'user_statuses', {user_id: userId, date: date, status_id: statusId});
    }

    function generateCalendar(year, month) {
      return HttpUtilities.get(serverLocation + 'generate?year='+year+'&month='+month);
    }

    function getAnalyticsReport(start, end) {
      $window.open(serverLocation + 'work_reports?start_date='+start+'&end_date='+end);
      $window.open(serverLocation + 'overall_reports?start_date='+start+'&end_date='+end);
    }

    return model;
  }

})();
