(function() {

    'use strict';

    angular
        .module('base')
        .factory('HttpUtilities', HttpUtilities);


    function HttpUtilities($http, $q) {
        var httpUtilities = {};

        httpUtilities.get = function(url) {
            var deferred = $q.defer();

            $http.get(url)
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(err, status) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        httpUtilities.post = function (url, data) {
            var deferred = $q.defer();

            $http(
              {
                method: 'POST',
                url: url,
                data: data
              })
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(err, status) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        httpUtilities.put = function(url, data) {
            var deferred = $q.defer();

            $http.put(url, data)
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(err, status) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        httpUtilities.delete = function(url) {
            var deferred = $q.defer();

            $http.delete(url)
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(err, status) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        return httpUtilities;
    }
})();
