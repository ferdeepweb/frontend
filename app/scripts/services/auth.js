(function() {

  'use strict';

  angular
    .module('base')
    .factory('AuthUtilities', AuthUtilities);


  function AuthUtilities($auth) {
    var AuthUtilities = {};

    AuthUtilities.isLoggedIn = function() {
      return $auth.validateUser();
    };

    AuthUtilities.signOut = function() {
      $auth.signOut()
        .then(function(resp) {
          // handle success response
        })
        .catch(function(resp) {
          alert("can't sign out");
          // handle error response
        });
    };

    return AuthUtilities;
  }
})();
