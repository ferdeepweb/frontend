angular.module('base').filter('highlight', function($sce) {
    return function(text, search) {
        if (!search || (search && search.length < 3)) {
            return $sce.trustAsHtml(text);
        }

        regexp  = '';

        try {
            regexp = new RegExp(search, 'gi');
        } catch(e) {
            return $sce.trustAsHtml(text);
        }
        if (text) {
            return $sce.trustAsHtml(text.replace(regexp, '<span class="highlightedText">$&</span>'));
        } else {
            return text;
        }
    };
});