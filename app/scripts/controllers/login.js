'use strict';

angular.module('base')
  .controller('LoginCtrl', function ($rootScope, $scope, $auth, $location) {
    $scope.userLogin = {
      email: '',
      password: ''
    };

    $scope.submitLogin = function() {
      $auth.submitLogin($scope.loginForm)
        .then(function(resp) {
          console.log('logirao se');
          $rootScope.$broadcast('successful_login');
          $location.path('/calendar');
        }, function(resp) {
          alert('User not found');
        });
    };
  });
