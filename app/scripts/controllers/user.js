'use strict';

angular.module('base')
  .controller('UserCtrl', function ($scope, proxy, $location) {

    $scope.userForm = {};
    $scope.userForm.job_ids = [];
    $scope.offices = [];

    proxy.getOffices()
      .then(function(data) {
        console.log('dobio', data);
        $scope.offices = data.offices;
      }, function(data) {
        alert('offices are not available');
      });

    $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.open = function($event, opened) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope[opened] = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.addJob = function(newJobId) {
      var exists = false;
      var existingId = -1;

      $scope.userForm.job_ids.forEach(function(jobId, idx) {
        if (newJobId === jobId) {
          exists = true;
          existingId = idx;
        }
      });

      if (!exists) {
        $scope.userForm.job_ids.push(newJobId);
      } else {
        $scope.userForm.job_ids.splice(existingId, 1);
      }
    };


    $scope.createUser = function(user) {
      console.log(user);

      proxy.createUser(user)
        .then(function(createdUser) {
          console.log(createdUser);
          $scope.userForm = {};
          $location.path('/employees');
          // Todo add toastr message
          alert('user successfully saved');
        }, function(data) {
          alert('failed saving user');
        });
    };

  });
