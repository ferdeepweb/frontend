'use strict';

angular.module('base')
  .controller('NavbarCtrl', function ($rootScope, $scope, AuthUtilities, $location) {

    var vm = this;

    vm.isLoggedIn = false;
    vm.isAdmin = false;
    vm.user = {};
    var successfullAuth = false;

    function getUser() {
      AuthUtilities.isLoggedIn()
        .then(function(data) {
          vm.isAdmin = data.role_id === 1;
          if (successfullAuth) {
            console.log('redirect');
            if (vm.isAdmin) {
              $location.path('/offices');
            } else {
              $location.path('/calendar').search({userId: data.id});
            }
            successfullAuth = false;
          }
          vm.isLoggedIn = true;
          vm.user = data;
        });
    }

    vm.signOut = function() {
      AuthUtilities.signOut();
      vm.isLoggedIn = false;
      vm.user = {};
      vm.isAdmin = false;
      $location.path('/');
    };

    $rootScope.$on('successful_login', function() {
      successfullAuth = true;
      getUser();
    });

    getUser();
  });
