'use strict';

angular.module('base')
  .controller('CalendarCtrl', function ($scope, proxy, $location) {

    $scope.schedule = [];
    $scope.loading = true;

    $scope.actionMenu = '<ul class="actions actions-alt" id="fc-actions">' +
      '<li class="dropdown" dropdown>' +
        '<a href="" dropdown-toggle><i class="zmdi zmdi-more-vert"></i></a>' +
        '<ul class="dropdown-menu dropdown-menu-right">' +
          '<li class="active">' +
            '<a data-calendar-view="month" href="">Month View</a>' +
          '</li>' +
          '<li>' +
            '<a data-calendar-view="agendaWeek" href="">Agenda Week View</a>' +
          '</li>' +
          '<li>' +
            '<a data-calendar-view="agendaDay" href="">Agenda Day View</a>' +
          '</li>' +
        '</ul>' +
      '</div>' +
    '</li>';

    var officeEvents = {};

    if ($location.search().userId) {
      proxy.getCalendarForUserAndDate($location.search().userId, 2016, 2)
        .then(function(data) {
          $scope.resultsLength = data.schedules.length;
          $scope.schedule = data.schedules;
          $scope.loading = false;
        })
    } else if ($location.search().officeId) {
      proxy.getCalendarForOfficeAndDate($location.search().officeId, 2016, 2)
        .then(function(data) {
          console.log(data);

          data.schedules.forEach(function(event) {

            var activeDate = event.active_on;
            var shift = event.shift_id;

            //console.log(activeDate, shift);

            if (officeEvents[activeDate] && officeEvents[activeDate][shift]) {
              officeEvents[activeDate][shift].jobs.concat(event.jobs);
              officeEvents[activeDate][shift].users.push(event.user);

            } else {
              officeEvents[activeDate] = officeEvents[activeDate] ? officeEvents[activeDate]: {};
              officeEvents[activeDate][shift] = {
                jobs: [],
                users: [],
                team: event.team
              };
              officeEvents[activeDate][shift].jobs.concat(event.jobs);
              officeEvents[activeDate][shift].users.push(event.user);
            }
          });

          console.log(officeEvents.length);
          $scope.resultsLength = Object.keys(officeEvents).length;
          //$scope.schedule = data.schedules;
          $scope.schedule = officeEvents;
          $scope.loading = false;
        })
    }



  });
