'use strict';

angular.module('base')
  .controller('UsersCtrl', function ($scope, proxy, $uibModal, $location) {

    $scope.users = [];
    proxy.listAllUsers()
      .then(function(users) {
        $scope.users = users.users;
        var searchedUserIdQuery = $location.search().userId;
        if (searchedUserIdQuery) {
          for (var i = 0; i < $scope.users.length; i++) {
            if ($scope.users[i].id == searchedUserIdQuery) {
              $scope.openEditUserModal($scope.users[i]);
              break;
            }
          }
        }
      });

    $scope.goToLocation = function(path, queryParam) {
      console.log('redirect');
      $location.path().search({userId: queryParam});
    };


    $scope.openEditUserModal = function(user) {
      var _modalInstance = $uibModal.open({
        animation: true,
        size: 'md',
        templateUrl: '/views/template/editUser.html',
        controller: function($scope, $location) {
          $scope.selectedUsers = user;
          $scope.redirect = function(path, queryParam) {
            console.log('redirect');
            $location.path().search({userId: queryParam});
          };

          $scope.today = function() {
            $scope.dt = new Date();
          };
          $scope.today();

          $scope.open = function($event, opened) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope[opened] = true;
          };

          $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
          };

          $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
          $scope.format = $scope.formats[0];
          $scope.statusForm = {};
          $scope.saveUserStatus = function() {
            for(var i = 0; i < $scope.statusForm.numberOfDays; i++) {

              var date = new Date();
              date.setDate($scope.statusForm.date.getDate() + i);
              proxy.saveUserStatus($scope.statusForm.status_id, date, user.id)
                .then(function(data) {
                  $scope.toggleStatus = false;
                  //$scope.statusForm = {};
                }, function() {
                  alert('saving failed');
                });
            }
          };

          $scope.seeCalendar = function(userId) {
            _modalInstance.dismiss('cancel');
            $location.path('/calendar').search({userId: user.id});
          };

          $scope.close = function() {
            _modalInstance.dismiss('cancel');
          };
        }
      });
    };
  });
