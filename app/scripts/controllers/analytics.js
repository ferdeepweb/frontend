'use strict';

angular.module('base')
  .controller('AnalyticsCtrl', function ($scope, proxy) {


    $scope.startDate;
    $scope.endDate;

    $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.open = function($event, opened) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope[opened] = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.submitAnalytics = function() {
      proxy.getAnalyticsReport(convertDate($scope.startDate), convertDate($scope.endDate));
    };

    function convertDate(oldDate) {
      var newDate = '';
      newDate += oldDate.getFullYear() + '-';
      newDate += oldDate.getMonth() + '-';
      newDate += oldDate.getDay();

      console.log(newDate);
      return newDate;
    }

  });
