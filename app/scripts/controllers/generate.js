'use strict';

angular.module('base')
  .controller('GenerateCtrl', function ($scope, proxy, $location) {
    $scope.generateForm = {
      month: 4,
      year: 2016
    };

    $scope.submitGenerate = function(generateForm) {
      proxy.generateCalendar(generateForm.year, generateForm.month)
        .then(function(data) {
          console.log(data);
          //$location.path('/offices');
        }, function(data) {
          alert('could not generate calendar for selected month');
        })
    };

  });
