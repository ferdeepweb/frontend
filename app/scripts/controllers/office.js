'use strict';

angular.module('base')
  .controller('NewOfficeCtrl', function ($scope, proxy, $location) {

    $scope.officeForm = {};

    $scope.createOffice = function(office) {
      proxy.createOffice(office)
        .then(function(createdOffice) {
          $location.path('/offices');
        }, function(data) {
          alert('failed saving user');
        });
    };

  }).controller('ListOfficeCtrl', function ($scope, proxy, $uibModal) {

  $scope.offices = [];

  function init() {
    proxy.listAllOffices()
      .then(function(data) {
        $scope.offices = data.offices;
      }, function(data) {
        alert('Can not get offices');
      });
  }

  $scope.openEditOfficeModal = function(office) {
    var _modalInstance = $uibModal.open({
      animation: true,
      size: 'md',
      templateUrl: '/views/template/editOffice.html',
      controller: function($scope, proxy, $location) {
        $scope.office = office;
        $scope.users = [];
        $scope.redirect = function(path, queryParam) {
          _modalInstance.dismiss('cancel');
          $location.path(path).search({userId: queryParam});
        };
        proxy.listUsersForOffice(office.id)
          .then(function(users) {
            $scope.users = users.offices;
          }, function(data) {
            console.log('nije uspio');
          });

        $scope.seeCalendar = function(officeId) {
          _modalInstance.dismiss('cancel');
          $location.path('/calendar').search({officeId: office.id});
        };

        $scope.close = function() {
          _modalInstance.dismiss('cancel');
        };
      }
    });
  };

  init();
});
