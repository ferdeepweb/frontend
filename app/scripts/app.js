'use strict';

angular
  .module('base', [
    'ngAnimate',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ng-token-auth'
  ])
  .config(function ($routeProvider, $locationProvider, $authProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        requireAuthentication: false,
        controller: 'MainCtrl'
      })
      .when('/calendar', {
        templateUrl: 'views/calendar.html',
        requireAuthentication: true,
        controller: 'CalendarCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        requireAuthentication: false,
        controller: 'LoginCtrl'
      })
      .when('/new-user', {
        templateUrl: 'views/createUser.html',
        requireAuthentication: true,
        controller: 'UserCtrl'
      })
      .when('/new-office', {
        templateUrl: 'views/office.html',
        requireAuthentication: true,
        controller: 'NewOfficeCtrl'
      })
      .when('/offices', {
        templateUrl: 'views/officeList.html',
        requireAuthentication: true,
        controller: 'ListOfficeCtrl'
      })
      .when('/employees', {
        templateUrl: 'views/users.html',
        requireAuthentication: true,
        controller: 'UsersCtrl'
      })
      .when('/generate', {
        templateUrl: 'views/generate.html',
        requireAuthentication: true,
        controller: 'GenerateCtrl'
      })
      .when('/analytics', {
        templateUrl: 'views/analytics.html',
        requireAuthentication: true,
        controller: 'AnalyticsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

      $authProvider.configure({
        //apiUrl: 'http://192.168.5.83:3000/api/v1'
        apiUrl: 'http://er.ddns.net:3000/api/v1'
      });
    })
  .run(function($rootScope, $location, AuthUtilities) {
    $rootScope.$on('$routeChangeStart', function(event, nextView) {
      // Auth wall
      if (nextView.requireAuthentication) {
        AuthUtilities.isLoggedIn()
          .then(function(data) {},
            function(data) {
            $location.url('/login');
          });
      }
    });
  });
